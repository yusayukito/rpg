using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLayers : MonoBehaviour
{
    // 壁判定のLayer
    [SerializeField] LayerMask soridObjectsLayer;
    [SerializeField] LayerMask interactableLayer;
    // エンカウント判定のLayer
    [SerializeField] LayerMask encountLayer;
    // 町判定のLayer
    [SerializeField] LayerMask worldLayer;
    [SerializeField] LayerMask natoVillageLayer;
    [SerializeField] LayerMask makiVillageLayer;
    // 家判定のLayer
    [SerializeField] LayerMask natoHouseLayer;
    [SerializeField] LayerMask natoOutLayer;

    [SerializeField] LayerMask makiHouseLayer;
    [SerializeField] LayerMask makiOutLayer;

    // どこからでも利用可能
    public static GameLayers Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    public LayerMask SoridObjectsLayer { get => soridObjectsLayer; }
    public LayerMask InteractableLayer { get => interactableLayer; }
    public LayerMask EncountLayer { get => encountLayer; }
    public LayerMask WorldLayer { get => worldLayer; }
    public LayerMask NatoVillageLayer { get => natoVillageLayer; }
    public LayerMask MakiVillageLayer { get => makiVillageLayer; }
    public LayerMask NatoHouseLayer { get => natoHouseLayer; }
    public LayerMask NatoOutLayer { get => natoOutLayer; }
    public LayerMask MakiHouseLayer { get => makiHouseLayer; }
    public LayerMask MakiOutLayer { get => makiOutLayer; }
}
