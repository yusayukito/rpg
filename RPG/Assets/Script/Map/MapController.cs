using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour
{
    [SerializeField] Character character;
    [SerializeField] GameObject worldMap;
    [SerializeField] GameObject natoVillage;
    [SerializeField] GameObject makiVillage;


    float x = -9.92f, y = -14.72f;

    public void Coordinate(float get_x, float get_y)
    {
        x = get_x;
        y = get_y;
    }

    public void WorldMap()
    {
        natoVillage.SetActive(false);
        makiVillage.SetActive(false);
        worldMap.SetActive(true);
        character.PlayerMove(x, y);
    }
    public void NatoVillage()
    {
        worldMap.SetActive(false);
        natoVillage.SetActive(true);
    }

    public void MakiVillage()
    {
        worldMap.SetActive(false);
        makiVillage.SetActive(true);
    }

    public void House()
    {
    }
}
