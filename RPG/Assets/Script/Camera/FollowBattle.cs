using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBattle : MonoBehaviour
{
    public GameObject player;
    //private Animator animator;

    void Start()
    {
        //animator = GetComponent<Animator>();
    }


    void Update()
    {
        Vector3 playerPos = player.transform.position;
        // カメラとプレイヤーの位置を同じにする
        transform.position = new Vector3(playerPos.x, playerPos.y, 0);


        //animator.SetFloat("multi", 0);
    }
}
