using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum BattleState
{
    Start,
    PlayerAction,
    PlayerMove,
    EnemyMove,
    Busy,
    Run,
}

public class BattleSystem : MonoBehaviour
{
    [SerializeField] BattleUnit playerUnit;
    [SerializeField] BattleUnit enemyUnit;
    [SerializeField] BattleHud playerHud;
    [SerializeField] BattleHud enemyHud;
    [SerializeField] BattleDialogBox dialogBox;

    // [SerializeField] GameController gameController;
    public UnityAction BattleOver;

    BattleState state;

    int currentAction;  // 0:たたかう,1:逃げる
    int currentMove;    // 0:左上, 1:右上, 2:左下, 3右下 の技

    public void StartBattle()
    {
        StartCoroutine(SetupBattle());
    }

    IEnumerator SetupBattle()
    {
        state = BattleState.Start;
        // モンスターの生成と描画
        playerUnit.Setup();
        enemyUnit.Setup();

        // HUDの描画
        playerHud.SetData(playerUnit.Monster);
        enemyHud.SetData(enemyUnit.Monster);
        dialogBox.SetMoveNames(playerUnit.Monster.Moves);
        yield return StartCoroutine(dialogBox.TypeDialog($"やせいの{enemyUnit.Monster.Base.Name}が現れた！"));

        PlayerAction();

    }

    void PlayerRun()
    {
        state = BattleState.Run;
        dialogBox.EnableActionSelector(false);
        ShowRunMessage();
      
    }

    void PlayerAction()
    {
        state = BattleState.PlayerAction;
        dialogBox.EnableActionSelector(true);
        StartCoroutine(dialogBox.TypeDialog($"行動を選択"));
    }

    void PlayerMove()
    {
        state = BattleState.PlayerMove;
        dialogBox.EnableDialogText(false);
        dialogBox.EnableActionSelector(false);
        dialogBox.EnableMoveSelector(true);
    }

    public void HandleUpdate()
    {
        if (state == BattleState.PlayerAction)
        {
            HandleActionSelection();
        }
        else if (state == BattleState.PlayerMove)
        {
            HandleMoveSelection();
        }
        else if (state == BattleState.Run)
        {
            ShowRunMessage();
        }
    }

    //PlaerMoveの実行
    IEnumerator PerformPlayerMove()
    {
        state = BattleState.Busy;

        //　技を決定
        Move move = playerUnit.Monster.Moves[currentMove];
        yield return StartCoroutine(dialogBox.TypeDialog($"{playerUnit.Monster.Base.Name}は{move.Base.Name}を使った"));

        // Enemyダメージ計算
        DamageDetails damageDetails = enemyUnit.Monster.TakeDamage(move, playerUnit.Monster);
        // HP反映
        yield return StartCoroutine(dialogBox.TypeDialog($"{damageDetails.Damage}ダメージ!"));
        //enemyHud.UpdateHP();
        yield return ShowDamageDetails(damageDetails);
        // 戦闘不能ならメッセージ
        if (damageDetails.Fainted)
        {
            yield return StartCoroutine(dialogBox.TypeDialog($"{enemyUnit.Monster.Base.Name}を倒した"));
            yield return new WaitForSeconds(0.7f);
            // gameController.EndBattle();
            BattleOver();
        }
        else
        {
            // それ以外ならEnemyMove
            StartCoroutine(EnemyMove());
        }
    }

    IEnumerator EnemyMove()
    {
        state = BattleState.EnemyMove;

        //　技を決定:ランダム
        Move move = enemyUnit.Monster.GetRandomMove();
        yield return StartCoroutine(dialogBox.TypeDialog($"{enemyUnit.Monster.Base.Name}は{move.Base.Name}を使った"));
        yield return new WaitForSeconds(1);

        // Enemyダメージ計算
        DamageDetails damageDetails = playerUnit.Monster.TakeDamage(move, enemyUnit.Monster);
        // HP反映
        yield return StartCoroutine(dialogBox.TypeDialog($"{damageDetails.Damage}ダメージ!"));
        //playerHud.UpdateHP();
        // 相性/クリティカルのメッセージ
        yield return ShowDamageDetails(damageDetails);
        // 戦闘不能ならメッセージ
        if (damageDetails.Fainted)
        {
            yield return StartCoroutine(dialogBox.TypeDialog($"{playerUnit.Monster.Base.Name}は倒れた"));
            yield return new WaitForSeconds(0.7f);
            // gameController.EndBattle();
            BattleOver();
        }
        else
        {
            // それ以外なら
            PlayerAction();

        }
    }

    IEnumerator ShowDamageDetails(DamageDetails damageDetails)
    {
        if (damageDetails.Critical > 1f)
        {
            yield return StartCoroutine(dialogBox.TypeDialog($"会心の一撃"));
        }
        if (damageDetails.TypeEffectivenss > 1f)
        {
            yield return StartCoroutine(dialogBox.TypeDialog($"効果バツグンだ"));
        }
        else if (damageDetails.TypeEffectivenss < 1f)
        {
            yield return StartCoroutine(dialogBox.TypeDialog($"効果はいまひとつ"));
        }

    }

    IEnumerator ShowRunMessage()
    {
        yield return StartCoroutine(dialogBox.TypeDialog($"{playerUnit.Monster.Base.Name}は逃げ出した"));
        yield return new WaitForSeconds(0.7f);
        BattleOver();
    }

    // PlayerActionでの行動を処理する
    void HandleActionSelection()
    {
        // 下を入力するとにげる,上を入力するとたたかうになる
        // 0:たたかう
        // 1:にげる
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (currentAction < 1)
            {
                currentAction++;

            }
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (currentAction > 0)
            {
                currentAction--;
            }
        }

        //色をつけてどちらを選択しているのかわかりやすくする
        dialogBox?.UpdateActionSelection(currentAction);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (currentAction == 0)
            {
                PlayerMove();
            }
            else if(currentAction == 1)
            {
                //  PlayerRun();
                BattleOver();
            }

        }
    }

    // 0:左上, 1:右上, 2:左下, 3右下 の技
    void HandleMoveSelection()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (currentMove < playerUnit.Monster.Moves.Count - 1)
            {
                currentMove++;

            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (currentMove > 0)
            {
                currentMove--;

            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (currentMove < playerUnit.Monster.Moves.Count - 2)
            {
                currentMove += 2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (currentMove > 0)
            {
                currentMove -= 2;
            }
        }
        dialogBox?.UpdateMoveSelection(currentMove, playerUnit.Monster.Moves[currentMove]);


        if (Input.GetKeyDown(KeyCode.Z))
        {
            // 技決定
            // ・技選択のUIは非表示
            dialogBox.EnableMoveSelector(false);
            // ・メッセージ復活
            dialogBox.EnableDialogText(true);
            // ・技決定の処理
            StartCoroutine(PerformPlayerMove());
        }
    }
/*
    void HandleSpellSelection()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (currentMove < playerUnit.Monster.Moves.Count - 1)
            {
                currentMove++;

            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (currentMove > 0)
            {
                currentMove--;

            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (currentMove < playerUnit.Monster.Moves.Count - 2)
            {
                currentMove += 2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (currentMove > 0)
            {
                currentMove -= 2;
            }
        }
        dialogBox?.UpdateMoveSelection(currentMove, playerUnit.Monster.Moves[currentMove]);


        if (Input.GetKeyDown(KeyCode.Z))
        {
            // 技決定
            // ・技選択のUIは非表示
            dialogBox.EnableMoveSelector(false);
            // ・メッセージ復活
            dialogBox.EnableDialogText(true);
            // ・技決定の処理
            StartCoroutine(PerformPlayerMove());
        }
    }*/
}
