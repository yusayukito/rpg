using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUnit : MonoBehaviour
{
    [SerializeField] MonsterBase _base; // 戦わせるモンスターをセットする
    [SerializeField] int level;

    public Monster Monster { get; set; }

    // バトルで使うモンスターを保持
    // モンスターの画像を反映する
    public void Setup()
    {
        // _baseからレベルに応じたモンスター
        // BattleSystemで使うからプロパティに入れる
        Monster = new Monster(_base, level);

        Image image = GetComponent<Image>();
        image.sprite = Monster.Base.FrontSprite;
    }
}
