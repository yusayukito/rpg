using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHud : MonoBehaviour
{
    [SerializeField] Text nameText;
    [SerializeField] Text levelText;
    [SerializeField] Text hpText;
    //    [SerializeField] HPBar hpBar;

    Monster _monster;

    public void SetData(Monster monster)
    {
        _monster = monster;
        nameText.text = monster.Base.Name;
        levelText.text = "LV " + monster.Level;
        hpText.text = "HP " + monster.HP;
    }

    /*
    public void UpdateHP() {
        hpBar.SetHP((float)_monster.HP / _monster.MaxHP); 
    }*/
}
