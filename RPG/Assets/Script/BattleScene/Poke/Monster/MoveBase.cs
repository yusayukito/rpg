using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MoveBase : ScriptableObject
{
    //　技のマスターデータ

    //　名前、詳細、タイプ、威力、正確性、PP

    [SerializeField] new string name;

    [TextArea]
    [SerializeField] string description;

    [SerializeField] MonsterType type;
    [SerializeField] float power;
    [SerializeField] int accuracy;
    [SerializeField] int pp;
    // プロパティ
    public string Name { get => name; }
    public string Description { get => description; }
    public float Power { get => power;  }

    public MonsterType Type { get => type;}
    public int PP { get => pp; }
    public int Accuracy { get => accuracy;}
}
