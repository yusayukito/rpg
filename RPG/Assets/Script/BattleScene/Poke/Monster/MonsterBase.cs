using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//モンスターのマスターデータ：外部から変更しない（インスペクターのみ変更可能）
[CreateAssetMenu]
public class MonsterBase : ScriptableObject
{
    // 名前,説明
    [SerializeField] new string name;
    [TextArea]
    [SerializeField] string description;
    // 画像
    [SerializeField] Sprite frontSprite;
    [SerializeField] Sprite backSprinte;
    // タイプ
    [SerializeField] MonsterType type1;
    [SerializeField] MonsterType type2;
    // ステータス
    [SerializeField] int maxHP;
    [SerializeField] int attack;
    [SerializeField] int defense;
    [SerializeField] int spAttack;
    [SerializeField] int spDefense;
    [SerializeField] int speed;

    // 覚える技一覧
    [SerializeField] List<LearnableMove> learnableMoves;

    public List<LearnableMove> LearnableMoves { get => learnableMoves; }
    public string Name { get => name; }
    public string Description { get => description; }
    public Sprite FrontSprite { get => frontSprite; }
    public Sprite BackSprinte { get => backSprinte; }
    public int MaxHP { get => maxHP; }
    public int Attack { get => attack; }
    public int Defense { get => defense; }
    public int SpAttack { get => spAttack; }
    public int SpDefense { get => spDefense; }
    public int Speed { get => speed; }
    public MonsterType Type1 { get;  }
    public MonsterType Type2 { get; }
}

//覚える技クラス
[Serializable]
public class LearnableMove
{
    [SerializeField] MoveBase _base;
    [SerializeField] int level;

    public MoveBase Base { get => _base; }
    public int Level { get => level; }

}

public enum MonsterType
{
    None,
    Normal,
    Fier,
    Water,
    Electric,
    Grass,
    Ice,
    Fighting,
    Poison,
    Ground,
    Flying,
    Psychic,
    Bug,
    Ghost,
    Dragon,

}


public class TypeChart
{
    static float[][] chart =
    {
        // 攻撃/防御        NOR   FIR   WAT   ELE   GRS   ICE   FIG   POI
        /*NOR*/ new float[]{1f,   1f,   1f,   1f,   1f,   1f,   1f,   1f},
        /*FIR*/ new float[]{1f, 0.5f, 0.5f,   1f,   2f,   2f,   1f,   1f},
        /*WAT*/ new float[]{1f,   2f, 0.5f,   1f, 0.5f,   1f,   1f,   1f},
        /*ELE*/ new float[]{1f,   1f,   2f, 0.5f, 0.5f,   1f,   1f,   1f},
        /*GRS*/ new float[]{1f, 0.5f,   2f,   1f, 0.5f,   1f,   1f, 0.5f},
        /*ICE*/ new float[]{1f, 0.5f, 0.5f,   1f,   2f, 0.5f,   1f,   1f},
        /*FIG*/ new float[]{2f,   1f,   1f,   1f,   1f,   2f,   1f, 0.5f},
        /*POI*/ new float[]{1f,   1f,   1f,   1f,   2f,   1f,   1f, 0.5f},
    };

    public static float GetEffectivenss(MonsterType attackType, MonsterType defenseType)
    {
        if(attackType == MonsterType.None || defenseType == MonsterType.None)
        {
            return 1f;
        }
        int row = (int)attackType - 1;
        int col = (int)defenseType - 1;
        return chart[row][col];
    }
}
