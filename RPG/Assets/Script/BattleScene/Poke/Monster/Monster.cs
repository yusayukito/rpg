using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//　レベルに応じたステータスの違うモンスターを生成するクラス
//　注意：データのみ扱う

public class Monster
{
    //　ベースとなるデータ
    public MonsterBase Base { get; set; }

    public int Level { get; set; }

    public int HP { get; set; }

    //　使える技
    public List<Move> Moves { get; set; }

    //　コンストラクタ
    public Monster(MonsterBase pBase, int pLevel)
    {
        Base = pBase;
        Level = pLevel;
        HP = Hp;

        Moves = new List<Move>();
        //使える技以上ならMovesに追加
        foreach (LearnableMove learnableMove in pBase.LearnableMoves)
        {
            if(Level >= learnableMove.Level)
            {
                Moves.Add(new Move(learnableMove.Base));
            }
        }
    }

    //　levelに応じたステータスを返すもの:プロパティ
    public int Attack
    {
        get { return Mathf.FloorToInt((Base.Attack * Level) / 100f) + 5; }
    }
    public int Defense
    {
        get { return Mathf.FloorToInt((Base.Defense * Level) / 100f) + 5; }

    }
    public int SpAttack
    {
        get { return Mathf.FloorToInt((Base.SpAttack * Level) / 100f) + 5; }

    }
    public int SpDefense
    {
        get { return Mathf.FloorToInt((Base.SpDefense * Level) / 100f) + 5; }

    }
    public int Hp
    {
        get { return Mathf.FloorToInt((Base.MaxHP * Level) / 100f) + 10; }

    }
 
    public int Speed
    {
        get { return Mathf.FloorToInt((Base.Speed * Level) / 100f) + 10; }

    }

    public DamageDetails TakeDamage(Move move,Monster attacker)
    {
        // クリティカル
        float critical = 1f;
        // 6.25%でクリティカル
        if (Random.value * 100 <= 6.25f)
        {
            critical = 2f;
        }
        // 相性
        float type = TypeChart.GetEffectivenss(move.Base.Type, Base.Type1) * TypeChart.GetEffectivenss(move.Base.Type, Base.Type2);
       

        float modifiers = Random.Range(0.85f, 1f) * type * critical;
        float a = (2 * attacker.Level + 10) / 250f;
        float d = a * move.Base.Power * ((float)attacker.Attack / Defense) + 2;
        int damage = Mathf.FloorToInt(d * modifiers);

        HP -= damage;

        DamageDetails damageDetails = new DamageDetails
        {
            Fainted = false,
            Critical = critical,
            TypeEffectivenss = type,
            Damage = damage
        };

        if (HP <= 0)
        {
            HP = 0;
            damageDetails.Fainted = true;
        }
        return damageDetails;
    }

    public Move GetRandomMove()
    {
        int r = Random.Range(0, Moves.Count);
        return Moves[r];
    }
}

public class DamageDetails
{
    public bool Fainted { get; set; }   //戦闘不能かどうか
    public float Critical { get; set; }
    public float TypeEffectivenss { get; set; }

    public int Damage { get; set; }

}
