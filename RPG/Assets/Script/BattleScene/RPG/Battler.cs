using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battler : MonoBehaviour
{
    // HPを持っている
    public new string name;
    public int hp;

    // 実行するコマンド
    public CommandSO selectCommand;
    public Battler target;
    public Battler enemy;

    // 持ってる技
    public List<CommandSO> commands = new List<CommandSO>();
    //持ってるアイテム
    public List<CommandSO> inventory = new List<CommandSO>();

    public string[] GetStringOfItems()
    {
        return GetStringOf(inventory);
    }

    public string[] GetStringOfCommands()
    {
        return GetStringOf(commands);
    }

    public string[] GetStringOf(List<CommandSO> commands)
    {
        List<string> list = new List<string>();
        foreach (CommandSO command in commands)
        {
            list.Add(command.name);
        }
        return list.ToArray();
    }


    public void SetTarget()
    {
        if (selectCommand.targetType == CommandSO.TargetType.Self)
        {
            target = this;
        }
        else if (selectCommand.targetType == CommandSO.TargetType.Enemy)
        {
            target = enemy;
        }
    }

    public void RemoveItem(CommandSO item)
    {
        inventory.Remove(item);
    }
}
