using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    //Battleで必要な変数をまとめたもの
    [SerializeField] BattleContext battlerContext;
    //　フェーズの多様化
    PhaseBase phaseState;

    void Start()
    {
        //phase = Phase.StartPhase;
        phaseState = new StartPhase();
        StartCoroutine(Battle());
    }

    IEnumerator Battle()
    {
        while (!(phaseState is EndPhase))
        {
            //　フェーズの実行
            yield return phaseState.Execute(battlerContext);
            //　次のフェーズに以降
            phaseState = phaseState.next;
        }
        // EndPhaseの実行
        yield return phaseState.Execute(battlerContext);

        yield break;    //　ここより下は実行しない

    }

    void Update()
    {
    }
}

//バトルに使う変数をまとめる：構造体（変数と関数をまとめたもの）
[System.Serializable]
public struct BattleContext
{

    //戦闘キャラクターを作りたい
    public Battler player;
    public Battler enemy;

    //　Window
    public WindowBattleMenuCommand windowBattleMenuCommand;
    public WindowBattleMenuCommand windowBattleSpellCommand;
    public WindowBattleMenuCommand windowBattleItemCommand;
    public BattleDialogBox battleDialogBox;

    public void SetEnemy()
    {
        player.enemy = enemy;
        enemy.enemy = player;
    }
};