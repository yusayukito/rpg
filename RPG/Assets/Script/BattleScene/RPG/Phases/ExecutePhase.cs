using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecutePhase : PhaseBase
{
    //数秒まち、受け付けない
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return null;
        battleContext.windowBattleMenuCommand.Close();
        battleContext.windowBattleSpellCommand.Close();

        battleContext.player.selectCommand.Execute(battleContext.player, battleContext.player.target);

        //プレイヤーのログメッセージ
        yield return new WaitForSeconds(1.0f);
        switch (battleContext.player.selectCommand.name)
        {
            case "攻撃":
                battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}の{battleContext.player.selectCommand.name}");
                break;
            case "回復":
                battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}の{battleContext.player.selectCommand.name}魔法");
                break;
            case "薬草":
                battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}は{battleContext.player.selectCommand.name}を使った");
                break;
        }
        battleContext.enemy.selectCommand.Execute(battleContext.enemy, battleContext.enemy.target);

        //エネミーのログメッセージ
        yield return new WaitForSeconds(1.0f);
        battleContext.battleDialogBox.SetDialog($"{battleContext.enemy.name}の{battleContext.enemy.selectCommand.name}");

        // どちらかが死亡したら
        if (battleContext.player.hp <= 0 || battleContext.enemy.hp <= 0)
        {
            next = new ResultPhase();
        }
        else
        {
            battleContext.windowBattleMenuCommand.Open();
            next = new ChooseCommandPhasem();
        }
        Debug.Log("ExecutePhase");
    }
}
