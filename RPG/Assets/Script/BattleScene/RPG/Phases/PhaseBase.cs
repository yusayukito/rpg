using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//継承用
public abstract　class PhaseBase 
{
    // このフェーズで何を実行数るのかの関数を持つ

    //　abstractは中身を持たない「抽象クラス」
    //　virtulは中身を持ってもいい
    
    //次のフェーズを変数として持つ
    public PhaseBase next;
    public abstract IEnumerator Execute(BattleContext battleContext);
}
