using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndPhase : PhaseBase
{
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return null;
        Debug.Log("EndPhase");
        //ここは戦闘画面がwindowタイプになった時に変更予定
        SceneManager.LoadScene("WorldScene");
    }
}
