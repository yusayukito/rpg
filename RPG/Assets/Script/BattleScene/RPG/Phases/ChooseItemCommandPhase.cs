using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseItemCommandPhase : PhaseBase
{
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return null;
        //　アイテム一覧を表示したい
        battleContext.windowBattleItemCommand.CreateSelectableText(battleContext.player.GetStringOfItems());

        //アイテムがなくなったら開かず藻い一度選択しなおし
        if (battleContext.player.inventory.Count != 0)
        {
            battleContext.windowBattleItemCommand.Open();
        }
        else
        {
            battleContext.windowBattleMenuCommand.Select();
            next = new ChooseCommandPhasem();
        }
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape));
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // とりあえずアイテムがなくなった時のエラーを消すため
            if (battleContext.player.inventory.Count == 1)
            {
                // 選択したアイテム
                int currentID = 0;
                // コマンドの設定
                battleContext.player.selectCommand = battleContext.player.inventory[currentID];
                // ターゲットの設定
                battleContext.player.SetTarget();
                //エネミー側のコマンド設定
                battleContext.enemy.selectCommand = battleContext.enemy.commands[0];
                battleContext.enemy.SetTarget();

                next = new ExecutePhase();
            }
            else if (battleContext.player.inventory.Count > 1)
            {
                // 選択したアイテム
                int currentID = battleContext.windowBattleItemCommand.currentID;
                // コマンドの設定
                battleContext.player.selectCommand = battleContext.player.inventory[currentID];
                // ターゲットの設定
                battleContext.player.SetTarget();
                //エネミー側のコマンド設定
                battleContext.enemy.selectCommand = battleContext.enemy.commands[0];
                battleContext.enemy.SetTarget();

                next = new ExecutePhase();
            }
        }
        else
        {
            battleContext.windowBattleMenuCommand.Select();
            next = new ChooseCommandPhasem();
        }


        battleContext.windowBattleItemCommand.gameObject.SetActive(false);

    }

}
