using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseCommandPhasem : PhaseBase
{
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return new WaitForSeconds(1.0f);
        battleContext.battleDialogBox.SetDialog($"どうする？");
        // 技選択をしたら次のフェーズにいく
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));  // KeyCodeは後で変更
                                                                            //Spaceを押すと
        int currentID = battleContext.windowBattleMenuCommand.currentID;
        if (currentID == 0)
        {
            //攻撃[0]なら攻撃
            // 技選択
            battleContext.player.selectCommand = battleContext.player.commands[0];
            battleContext.player.target = battleContext.enemy;
            battleContext.enemy.selectCommand = battleContext.enemy.commands[0];
            battleContext.enemy.target = battleContext.player;
            next = new ExecutePhase();
        }
        else if (currentID == 1)
        {
            next = new ChooseSpellCommandPhase();
        }
        else if (currentID == 2)
        {

            battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}は逃げたした");
            yield return new WaitForSeconds(1.0f);
            // 逃げるコマンド　いったん
            SceneManager.LoadScene("WorldScene");            
        }
        else if (currentID == 3)
        {
            //アイテム使用
            next = new ChooseItemCommandPhase();
        }
        else
        {
            //それ以外なら再度ChooseCommandPhasemに移る
            next = new ChooseCommandPhasem();
        }

        Debug.Log("ChooseCommandPhasem");

    }
}
