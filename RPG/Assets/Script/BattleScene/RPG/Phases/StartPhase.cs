using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPhase : PhaseBase
{
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return null;
        Debug.Log("StartPhase");
        battleContext.SetEnemy();
        battleContext.windowBattleMenuCommand.Open();
        battleContext.battleDialogBox.SetDialog($"{battleContext.enemy.name}�����ꂽ");
        next = new ChooseCommandPhasem();
    }
}
