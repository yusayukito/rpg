using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBMap;


public class ResultPhase : PhaseBase
{
    public override IEnumerator Execute(BattleContext battleContext)
    {
        yield return null;
        if (battleContext.player.hp <= 0)
        {
            battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}は敗北した");
        }else if(battleContext.enemy.hp <= 0)
        {
            battleContext.battleDialogBox.SetDialog($"{battleContext.player.name}は勝利した");
        }

        //前回のワールドマップに戻る
       // WorldSaveSystem.Instance.WorldLoads();
        Debug.Log("ResultPhase");
        next = new EndPhase();
    }
}
