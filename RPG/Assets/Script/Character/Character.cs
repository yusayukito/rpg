using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    // 移動/アニメーションを管理する

    [SerializeField] MapController mapController;

    [SerializeField] float moveSpeed;

    CharacterAnimator animator;

    public bool IsMoving { get; set; }

    public CharacterAnimator Animator { get => animator; }

    private void Awake()
    {
        animator = GetComponent<CharacterAnimator>();
    }

    // コルーチンを使って徐々に目的地に近づける
    public IEnumerator Move(Vector2 MoveVec, UnityAction OnMoveOver = null)
    {
        // 向きを変えたい
        animator.MoveX = MoveVec.x;
        animator.MoveY = MoveVec.y;
        Vector3 targetPos = transform.position;
        targetPos += (Vector3)(MoveVec * 0.64f);
        if (!IsWalkable(targetPos))
        {
            yield break;
        }

        // 移動中は入力を受け付けたくない
        IsMoving = true;

        // targetPosとの左があるなら繰り返す
        while ((targetPos - transform.position).sqrMagnitude > Mathf.Epsilon)
        {
            // targetPosに近づける
            transform.position = Vector3.MoveTowards(
                transform.position, // 現在の場所
                targetPos,          // 目的地
                moveSpeed * Time.deltaTime
                );
            yield return null;
        }


        transform.position = targetPos;
        IsMoving = false;
        OnMoveOver?.Invoke();
        CheckForCity();
        CheckForHouse();
    }

    public void HandleUpdate()
    {
        animator.IsMoving = IsMoving;
    }

    // targetPosに移動可能かを調べる関数
    bool IsWalkable(Vector2 targetPos)
    {
        // targetPosに0.2fの円のRayを飛ばして、ぶつからなかった
        return Physics2D.OverlapCircle(targetPos, 0.2f, GameLayers.Instance.SoridObjectsLayer | GameLayers.Instance.InteractableLayer) == false;
    }

    // 自分の場所から、円のRayを飛ばして、マップ移動のLayerに当たったら、画像を切り替える
    void CheckForCity()
    {
        // 各町からワールドマップにでるLayerに当たったら
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.WorldLayer))
        {
            mapController.WorldMap();
        }
        // ナトの村のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.NatoVillageLayer))
        {
            mapController.Coordinate(-9.92f, -14.72f);
            mapController.NatoVillage();
            PlayerMove(64f, 59.52f);
        }
        // マキ村のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.MakiVillageLayer))
        {
            mapController.Coordinate(0.96f, -8.32f);
            mapController.MakiVillage();
            PlayerMove(-0.96f, -5.44f);
        }
    }

    void CheckForHouse()
    {
        // ナトの里の家のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.NatoHouseLayer))
        {
            PlayerMove(0.96f, -2.24f);
        }
        // ナトの里の家の出口のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.NatoOutLayer))
        {
            PlayerMove(64.64f, 64.64f);
        }

        // マキ村の家のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.MakiHouseLayer))
        {
            PlayerMove(65.92f, 59.52f);
        }
        // マキ村家の出口のLayer
        if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.MakiOutLayer))
        {
            PlayerMove(-3.52f, 2.88f);
        }
    }

    public void PlayerMove(float get_x, float get_y)
    {
        transform.position = new Vector2(get_x, get_y);
    }
}
