using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using WBMap;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{

    Vector2 input;

    Character character;
    public UnityAction OnEncounted;
   // [SerializeField] GameController gameController;

    /*
    //　試験用
    [SerializeField] WorldData worldData;


    //保存用のソース
    [SerializeField]
    private MyStatus status;
    
        // Menuの表示用
        [SerializeField]
        GameObject menuCanvas;

        public void HandleStart()
        {
            menuCanvas = menuCanvas.transform.Find("MenuCanvas").gameObject;
        }
    */

    private void Awake()
    {
        character = GetComponent<Character>();
    }


    public void HandleUpdate()
    {
        if (!character.IsMoving)
        {
            // キーボードの入力方向に動く
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = Input.GetAxisRaw("Vertical");

            // 斜め移動禁止
            if (input.x != 0)
            {
                input.y = 0;
            }

            // 入力があったら
            if (input != Vector2.zero)
            {
                StartCoroutine(character.Move(input, CheckForEncounters));
                //StartCoroutine(character.Move(input, CheckForCity));
                //StartCoroutine(character.Move(input, CheckForHouse));
            }
        }
        character.HandleUpdate();

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Interact();
        }
        /*
        //セーブするときの例
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SaveSystem.Instance.Save(status);
            WorldSaveSystem.Instance.WorldSaves(worldData);
            Debug.Log("セーブしました");
            Debug.Log(this.transform.position);
        }

        //ロードの例
        if (Input.GetKeyDown(KeyCode.L))
        {
            status = SaveSystem.Instance.Load();
            WorldSaveSystem.Instance.WorldLoads();
            Debug.Log("ロードしました");
        }

        
        //メニューの表示
        if (Input.GetKeyDown(KeyCode.M))
        {
            menuCanvas.SetActive(true);
        }
        */

        // 自分の場所から、Rayを飛ばして、会話のLayerに当たったら、UI表示
        void Interact()
        {
            // 向いてる方向
            Vector3 faceDirection = new Vector3(character.Animator.MoveX * 0.64f, character.Animator.MoveY * 0.64f);
            // 干渉する場所
            Vector3 interactPos = transform.position + faceDirection;

            // 干渉する場所にRayを飛ばす
            Collider2D collider2D = Physics2D.OverlapCircle(interactPos, 0.2f, GameLayers.Instance.InteractableLayer);
            if (collider2D)
            {
                collider2D.GetComponent<IInteractable>()?.Interact();
            }
        }

        

        // 自分の場所から、円のRayを飛ばして、エンカウントのLayerに当たったら、ランダムエンカウント
        void CheckForEncounters()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.2f, GameLayers.Instance.EncountLayer))
            {
                // ランダムエンカウント
                if (Random.Range(0, 100) < 10)
                {
                    //  SceneManager.LoadScene("BattleScene");
                    OnEncounted();
                    //アニメーションのバグを修正
                }
            }
        }
    }
}