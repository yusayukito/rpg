using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;


[Serializable]
public class SaharadaStatus : MonoBehaviour
{
    //キャラの名前
    [SerializeField]
    private string characterName = "";
    //触手獣サハラダレベル
    [SerializeField]
    private int level = 7;
    //HP
    [SerializeField]
    private int hp = 700;
    //MP
    [SerializeField]
    private int mp = 0;
    //攻撃
    [SerializeField]
    private int power = 20;
    //素早さ
    [SerializeField]
    private int speed = 18;

    //　便利になるための改良   Getter,setterをpropertyで管理
    public string Name { get => characterName; set => characterName = value; }
    public int Level { get => level; set => level = value; }
    public int Hp { get => hp; set => hp = value; }
    public int Mp { get => mp; set => mp = value; }
    public int Power { get => power; set => power = value; }
    public int Speed { get => speed; set => speed = value; }

    private float actionTime;

    public ENEMY_STATE enemyState;

    public enum ENEMY_STATE
    {
        SET_UP
    }

    //経験値の最小
    private int minExp;
    //経験値の最大
    private int maxExp;

    private int exp;

    void start()
    {
        SetUpEnemyParameter();
    }

    private void SetUpEnemyParameter()
    {
        enemyState = ENEMY_STATE.SET_UP;


        //経験値を設定
        exp = UnityEngine.Random.Range(minExp, maxExp);
        actionTime = UnityEngine.Random.Range(100, 150);
    }

    void Update()
    {
        // 準備状態。敵のパラメータの準備が終了していない場合にはUpdateを処理しない
        if (enemyState == ENEMY_STATE.SET_UP)
        {
            return;
        }
    }

    private void TakeDamege(MyStatus myStatus)
    {
        //残りHPの確認
        if (hp <= 0)
        {
            //倒したらプレイヤーに経験値を加算する
            myStatus.AddExp((int)Mathf.Floor(exp * 20));
        }
    }
}
