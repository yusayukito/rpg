using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;

[Serializable]
public class TokiStatus : MonoBehaviour
{
    //キャラの名前
    [SerializeField]
    private string characterName = "";
    //キャラクターレベル
    [SerializeField]
    private int level = 1;
    //HP
    [SerializeField]
    private int hp = 9;
    //MP
    [SerializeField]
    private int mp = 6;
    //攻撃
    [SerializeField]
    private int power = 4;
    //素早さ
    [SerializeField]
    private int speed = 4;

    //　便利になるための改良   Getter,setterをpropertyで管理
    public string Name { get => characterName; set => characterName = value; }
    public int Level { get => level; set => level = value; }
    public int Hp { get => hp; set => hp = value; }
    public int Mp { get => mp; set => mp = value; }
    public int Power { get => power; set => power = value; }
    public int Speed { get => speed; set => speed = value; }

    //獲得した経験値
    public int exp;

    //次のレベルアップまでに必要な経験値
    public int nextLevelExp;

    //レベルアップ時に上がるパラメータの最低値
    private const int MIN_ABILITY_VALUE = 3;
    //レベルアップ時に上がるパラメータの中央値
    private const int MEDIAN_ABILITY_VALUE = 4;
    //レベルアップ時に上がるパラメータの最大値
    private const int MAX_ABILITY_VALUE = 5;


    //レベルの限界
    private const int MAX_LEVEL = 25;

    void Statt()
    {
        //レベルアップに必要な経験値を算出
        nextLevelExp = GetNextLevelExp();
    }

    //レベルアップに必要な経験値を算出する
    private int GetNextLevelExp()
    {
        return (100 * level * level);
    }

    //経験値を加算する
    public void AddExp(int enemyExp)
    {
        exp += enemyExp;

        //レベルが最大でなければレベルアップするかのチェックをする
        if (level < MAX_LEVEL)
        {
            UpdateLevel();
        }
    }

    //レベルアップのチェック
    private void UpdateLevel()
    {
        if (exp < nextLevelExp)
        {
            //経験値が目標値を超えていなければレベルアップしない
            return;
        }

        //レベルを加算
        level++;
        nextLevelExp = GetNextLevelExp();

        //能力値の上昇
        UpdateParametar();

        //レベルが最大でなければ追加のレベルアップのチェック処理
        if (level < MAX_LEVEL)
        {
            UpdateLevel();
        }
    }

    //能力の上昇
    private void UpdateParametar()
    {
        hp += UnityEngine.Random.Range(MAX_ABILITY_VALUE, MAX_ABILITY_VALUE);
        mp += UnityEngine.Random.Range(MEDIAN_ABILITY_VALUE, MEDIAN_ABILITY_VALUE);
        power += UnityEngine.Random.Range(MEDIAN_ABILITY_VALUE, MEDIAN_ABILITY_VALUE);
        speed += UnityEngine.Random.Range(MIN_ABILITY_VALUE, MIN_ABILITY_VALUE);

    }
}


