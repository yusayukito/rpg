//いったん後回し

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;


//のちにこれらを継承予定

//Serializable
//jsonで保存したいクラスには必要
[Serializable]
public class Status
{
    //SerializeField
    //保存したいデータがprivateのときに付けとかないといけない

    //キャラの名前
    [SerializeField]
    protected string characterName = "";
    //キャラクターレベル
    [SerializeField]
    protected int level;
    //HP
    [SerializeField]
    protected int hp;
    //MP
    [SerializeField]
    protected int mp;
    //攻撃
    [SerializeField]
    protected int power;
    //素早さ
    [SerializeField]
    protected int speed;

    //　便利になるための改良   Getter,setterをpropertyで管理
    public string Name { get => characterName; set => characterName = value; }
    public int Level { get => level; set => level = value; }
    public int Hp { get => hp; set => hp = value; }
    public int Mp { get => mp; set => mp = value; }
    public int Power { get => power; set => power = value; }
    public int Speed { get => speed; set => speed = value; }

    public static implicit operator Status(List<Status> v)
    {
        throw new NotImplementedException();
    }
}
