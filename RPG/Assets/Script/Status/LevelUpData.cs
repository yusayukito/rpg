//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;


////主人公のレベルアップデータ
////probabilityと名のついたのはステータスがレベルアップ時に上がる確率
////maxとついているのはレベルアップ時に上がるステータスの最高値
////CreateAssetMenu属性を使用することで`Assets > Create >ScriptableObjects >CreateLevelUpData'という項目が表示される
////これを押すとこの'CreateLevelUpDataが'LevelUpData'という名前でアセット化されてassetsフォルダに入る
//[Serializable]
//[CreateAssetMenu(fileName = "LevelUpData", menuName = "ScriptableObjects / LevelUpData")]
//public class LevelUpData : ScriptableObject
//{
//    //レベルアップに必要な経験値
//    [SerializeField]
//    private LevelUpDictionary requiredExperience = null;
//    ////レベルアップ時にHPの最大値が上がる確率
//    //[SerializeField]
//    //private float probabilityToIncreaseHP = 100f;
//    ////レベルアップ時にMPの最大値が上がる確率
//    //[SerializeField]
//    //private float probabilityToIncreaseMP = 100f;
//    ////レベルアップ時に攻撃の最大値が上がる確率
//    //[SerializeField]
//    //private float probabilityToIncreasePower = 100f;
//    ////レベルアップ時に素早さの最大値が上がる確率
//    //[SerializeField]
//    //private float probabilityToIncreaseSpeed = 100f;

//    //レベルアップによりHPが上がる時の値
//    [SerializeField]
//    private int HPRisingLimit = 5;
//    //MP
//    [SerializeField]
//    private int MPRisingLimit = 3;
//    //攻撃
//    [SerializeField]
//    private int PowerRisingLimit = 4;
//    //素早さ
//    [SerializeField]
//    protected int SpeedRisingLimit = 3;


//    //このレベルに必要な経験値
//    public int GetRequiredExperience(int level)
//    {
//        return requiredExperience.Keys.Contains(level) ? requiredExperience[level] : int.MaxValue;
//    }
//    public LevelUpDictionary GetLevel()
//    {
//        return requiredExperience;
//    }
//    //public float GetProbabilityToIncreaseHP()
//    //{
//    //    return ProbabilityToIncreaseHP;
//    //}
//    //public float GetProbabilityToIncreaseMP()
//    //{
//    //    return ProbabilityToIncreaseMP;
//    //}
//    //public float GetProbabilityToIncreasePower()
//    //{
//    //    return ProbabilityToIncreasePower;
//    //}
//    //public float GetProbabilityToIncreaseSpeed()
//    //{
//    //    return ProbabilityToIncreaseSpeed;
//    //}



//    public int GetHPRisingLimit()
//    {
//        return HPRisingLimit;
//    }
//    public int GetMPRisingLimit()
//    {
//        return MPRisingLimit;
//    }
//    public int GetPowerRisingLimit()
//    {
//        return PowerRisingLimit;
//    }
//    public int GetSpeedRisingLimit()
//    {
//        return SpeedRisingLimit;
//    }
//}