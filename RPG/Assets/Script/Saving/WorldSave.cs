using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
//using System.Linq;
using System.Text;

namespace WBMap
{
    /*jsonを使用しセーブしたい情報をクラスとして保存し、ロードするときはクラスとして読み込む*/
    public class WorldSaveSystem
    {
        #region Singleton
        private static WorldSaveSystem instance = new WorldSaveSystem();

        public static WorldSaveSystem Instance => instance;
        #endregion

       // private WorldSaveSystem() { WorldLoads(); }

        public string Path => Application.dataPath + "/worldData.json";

        //シーン上のプレイヤーの位置情報を取得するため
        GameObject gameObject, map;
        GameObject worldMap,makiVillage;
        //MapController map;


        public void WorldSaves(WorldData world)
        {
            gameObject = GameObject.Find("Daito");
            map = GameObject.Find("MapController");
            worldMap = GameObject.Find("WorldMap");
            makiVillage = GameObject.Find("MakiVillage");

            //プレイヤーの現在位置を取得
            world.WorldPos_x = gameObject.transform.position.x;
            world.WorldPos_y = gameObject.transform.position.y;

            if (worldMap.activeSelf == true)
            {
                world.Area = 0;
            }
            else if(makiVillage.activeSelf == true)
            {
                world.Area = 1;
            }
            string jsonData = JsonUtility.ToJson(world);
            StreamWriter writer = new StreamWriter(Path, false);
            writer.WriteLine(jsonData);
            writer.Flush();
            writer.Close();
        }

        public WorldData WorldLoads()
        {
            if (!File.Exists(Path))
            {
                Debug.Log("初期起動");
                return new WorldData();

            }

            StreamReader reader = new StreamReader(Path);
            string jsonData = reader.ReadToEnd();
            var data = JsonUtility.FromJson<WorldData>(jsonData);
            reader.Close();

            gameObject = GameObject.Find("Daito");
            map = GameObject.Find("MapController");

            switch (data.Area)
            {
                case 0:
                    map.GetComponent<MapController>().WorldMap();
                    break;
                case 1:
                    map.GetComponent<MapController>().MakiVillage();
                    break;
            }

            Vector3 pos = new Vector3(data.WorldPos_x,data.WorldPos_y);
            gameObject.transform.position = pos;

            return data;
        }


    }
}
