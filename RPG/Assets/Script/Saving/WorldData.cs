using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;

enum area{
    world,  //　0
    village,
    }

//のちにこれらを継承予定

//Serializable
//jsonで保存したいクラスには必要
[Serializable]
public class WorldData
{

    //現在のいるエリア
    [SerializeField]
    int area;

    // 現在の座標
    [SerializeField]
    private Vector3 WorldPos;
    [SerializeField] 
    public int Area { get => area; set => area = value; }
    [SerializeField] 
    public float WorldPos_x { get => WorldPos.x; set => WorldPos.x = value; }
    [SerializeField] 
    public float WorldPos_y { get => WorldPos.y; set => WorldPos.y = value; }
}
