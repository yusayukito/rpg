using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace WBMap
{

    [System.Serializable]   

    /*この配列はセーブしたい項目を保存するクラス*/
    public class UserData
    {
        public MyStatus myStatus = new MyStatus();
        public string UserName = "Default Name";
        public int UserLevel = -1;
    }
}
