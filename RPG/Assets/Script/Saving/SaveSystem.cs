using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
//using System.Linq;
using System.Text;

namespace WBMap
{
    /*jsonを使用しセーブしたい情報をクラスとして保存し、ロードするときはクラスとして読み込む*/
    public class SaveSystem
    {
        #region Singleton
        private static SaveSystem instance = new SaveSystem();

        public static SaveSystem Instance => instance;
        #endregion

        private SaveSystem() { Load(); }

        public string Path => Application.dataPath + "/data.json";

        //セーブしたいときはこれを呼び出せばいい
        //引数：保存したクラス
        //戻り値：なし
        public void Save(MyStatus status)
        {
         
            string jsonData = JsonUtility.ToJson(status);
            StreamWriter writer = new StreamWriter(Path, false);
            writer.WriteLine(jsonData);
            writer.Flush();
            writer.Close();
        }


        //ロードしたいときはこれを呼び出せばいい
        //引数：なし
        //戻り値：jsonのファイルの中身
        public MyStatus Load()
        {
            if (!File.Exists(Path))
            {
                Debug.Log("初期起動");
                return new MyStatus();

            }

            StreamReader reader = new StreamReader(Path);
            string jsonData = reader.ReadToEnd();
            var data = JsonUtility.FromJson<MyStatus>(jsonData);
            reader.Close();
           
            return data;
        }

      
    }
}
