using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WBMap
{
    public class SaveTest : MonoBehaviour
    {
        [SerializeField]
        private MyStatus status;

        void Update()
        {
            //セーブするときの例
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SaveSystem.Instance.Save(status);
                Debug.Log("セーブしました");
            }

            //ロードの例
            if (Input.GetKeyDown(KeyCode.L))
            {
                status = SaveSystem.Instance.Load();
                Debug.Log("ロードしました");
            }


            //ここからセーブの確認用

            //呼び出したいクラスは　
            //クラスのインスタンス.データ
            //で呼び出せる

            if (Input.GetKeyDown(KeyCode.C))
            {
                Debug.Log("レベルの表示");
                Debug.Log("User Rank =" + status.Level);

            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                status.Level++;
                Debug.Log("ランクアップ");
            }

            //ここまで
        }
    }
}

