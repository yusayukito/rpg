using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class HealItemSO : CommandSO
{
    [SerializeField] int healPoint;
    public override void Execute(Battler user, Battler target)
    {
        target.hp += healPoint;
        Debug.Log($"òÌgp{target.name}ð{healPoint}ñ:cèHP{target.hp}");
        if (user.inventory.Count() - 1 == 0)
        {
            user.inventory = null;
        }
        else
        {
            user.RemoveItem(this);
        }
    }
}
