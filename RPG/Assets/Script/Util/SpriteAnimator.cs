using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator
{
    // 描画するためのRenderer
    SpriteRenderer spriteRenderer;
    // 表示する画像達
    List<Sprite> frames;
    // フレームレート:どのタイミングで画像を切り替えるのか？
    float frameRate;

    // 現在のフレーム:何枚目の画像を表示する
    int currentFreme;
    float timer;

    public List<Sprite> Frames { get => frames; }

    // 初期化
    public SpriteAnimator(SpriteRenderer spriteRenderer, List<Sprite> frames, float frameRate = 0.16f)
    {
        this.spriteRenderer = spriteRenderer;
        this.frames = frames;
        this.frameRate = frameRate;
    }

    // アニメーション開始
    public void Start()
    {
        currentFreme = 0;
        timer = 0;
        spriteRenderer.sprite = frames[currentFreme];
    }

    // アニメーションを更新する:timerがframeRateを越えたら次の画像
    public void HandleUpdate()
    {
        timer += Time.deltaTime;
        if(timer > frameRate)
        {
            currentFreme = (currentFreme + 1) % frames.Count;
            spriteRenderer.sprite = frames[currentFreme];
            timer -= frameRate;
        }
    }
}
